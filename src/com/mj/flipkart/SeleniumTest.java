package com.mj.flipkart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class SeleniumTest {

    WebDriver webDriver;
    WebElement removePopUP ;
    WebElement loginButton;
    WebElement mobileNO;
    WebElement password;
    WebElement getLoginButton;
    WebElement productName;
    WebElement searchButton;
    WebElement getElement;
    WebElement newUser;
    WebElement continueButton;
    WebElement addToCart;
    WebElement placeOrder;

    public void invokeBrowser() {
        try {
            System.setProperty("webdriver.chrome.driver","/home/ubuntu/Desktop/Flipkart_Project/src/Data/chromedriver");
            webDriver = new ChromeDriver();
            webDriver.manage().window().maximize();
            webDriver.manage().deleteAllCookies();
            webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
            webDriver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(30));
            webDriver.get("https://www.flipkart.com/");
            registerNewUser();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registerNewUser(){
        try {
            removePopUP = webDriver.findElement(By.xpath("//button[contains(text(),'✕')]"));
            removePopUP.click();
            Thread.sleep(2000);
            loginButton = webDriver.findElement(By.xpath("//a[contains(text(),'Login')]"));
            loginButton.click();
            Thread.sleep(2000);
            newUser = webDriver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[6]/a"));
            newUser.click();
            Thread.sleep(2000);
            mobileNO = webDriver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[1]/input[1]"));
            mobileNO.sendKeys("9521023436");
            Thread.sleep(2000);
            continueButton = webDriver.findElement(By.xpath("/html/body/div[2]/div/div/div/div/div[2]/div/form/div[3]/button"));
            continueButton.click();
            Thread.sleep(2000);
            login();
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void login() {
        try {
            password = webDriver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[2]/input[1]"));
            password.sendKeys("jain123");
            Thread.sleep(2000);
            getLoginButton = webDriver.findElement(By.xpath("/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/form[1]/div[4]/button[1]"));
            getLoginButton.click();
            Thread.sleep(2000);
            addToCart();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void addToCart(){
        try {
            productName = webDriver.findElement(By.className("_3704LK"));
            productName.sendKeys("iphone");
            Thread.sleep(2000);
            searchButton = webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div[1]/div[1]/div[2]/div[2]/form/div/button"));
            searchButton.click();
            Thread.sleep(2000);
            getElement = webDriver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[3]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/a[1]/div[2]/div[1]/div[1]"));
            getElement.click();
            Thread.sleep(2000);

            List<String> newPageData = new ArrayList<>(webDriver.getWindowHandles());
            addToCart = webDriver.switchTo().window(newPageData.get(1)).findElement(By.xpath("//*[@id=\"container\"]/div/div[3]/div[1]/div[1]/div[2]/div/ul/li[1]/button"));
            addToCart.click();
            Thread.sleep(2000);
            placeTheOrder();

        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void placeTheOrder(){
        try {
            placeOrder = webDriver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div[1]/div/div[3]/div/form/button/span"));
            placeOrder.click();
            Thread.sleep(5000);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        SeleniumTest webpage = new SeleniumTest();
        webpage.invokeBrowser();
    }
}

